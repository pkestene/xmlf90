.SUFFIXES: 
.SUFFIXES: .f .F .o .a  .f90 .F90
#---------------------------
XMLF90_ROOT ?= ../../..
#
# Get building rules for this compiler
#
include $(XMLF90_ROOT)/fortran.mk
#
# Find appropriate modules and libraries
#
include $(XMLF90_ROOT)/xmlf90.mk
#
# The XMLF90_ variables are set by the xmlf90.mk file
#
INCFLAGS:= $(XMLF90_INCFLAGS) $(INCFLAGS)
LIBS:= $(XMLF90_LIBS) $(LIBS)
#---------------------------
